import PhotoSwipeLightbox from './photoswipe-lightbox.esm.min.js';
const lightbox = new PhotoSwipeLightbox({
    gallery: '#albumList',
    children: 'a',
    // dataSource: [
    //     { src: 'https://dummyimage.com/800x600/555/fff/?text=1', width: 800, height: 600 },
    //     { src: 'https://dummyimage.com/800x600/555/fff/?text=2', width: 800, height: 600 },
    //     { src: 'https://dummyimage.com/800x600/555/fff/?text=3', width: 800, height: 600 },
    //     { src: 'https://dummyimage.com/800x600/555/fff/?text=4', width: 800, height: 600 },
    //     { src: 'https://dummyimage.com/800x600/555/fff/?text=5', width: 800, height: 600 },
    // ],
    pswpModule: () => import('./photoswipe.esm.min.js')
});
lightbox.init();
console.log($('#albumList').find('a').length);

//=== 如果要使用onclick觸發 需使用 dataSource ===
// document.querySelector('#btnTest').onclick = () => {
//     lightbox.loadAndOpen(2);
// };