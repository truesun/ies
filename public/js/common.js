var hamSwitch = false;
var windowRW = 1024;
$(document).ready(function () {
    var windowWAct;
    var navHoverList = ['navAbout', 'navNews', 'navSpeech', 'navMember', 'navStudy', 'navInfo', 'navLibrary', 'navPromote', 'navInner'];
    $.each(navHoverList, function (key, val) {
        var $hoverList = $('#' + val + 'List');
        // $hoverList.css({ 'top': '-10px' });
        $('#' + val + 'Btn, #' + val + 'List').on('mouseenter', function () {
            windowWAct = $(window).width();
            if (windowWAct > windowRW) {
                var marginTop = $('.header-wrap').outerHeight();
                $hoverList.stop().animate({ top: marginTop - 1 }, 200);
                //= 上atc三角形 =
                $('#' + val + 'Btn').addClass('act');
            }
        });
        $('#' + val + 'Btn, #' + val + 'List').on('mouseleave', function () {
            windowWAct = $(window).width();
            if (windowWAct > windowRW) {
                $hoverList.stop().animate({ top: '20px' }, 200);
                //= 上atc三角形 =
                $('#' + val + 'Btn').removeClass('act');
            }
        });
    });

    //=== 漢堡 ===
    var $hamburger = $("#hamBtn");
    var $navItem = $('#navItem');
    var windowW = $(window).width();

    if (windowW < windowRW) {
        $navItem.css({ 'display': 'none' });
    } else {
        $navItem.css({ 'display': 'flex' });
    }
    $hamburger.on("click", function (e) {
        if (hamSwitch) {
            $hamburger.removeClass("is-active");
            // $navItem.css({ 'display': 'flex' });
            $navItem.slideUp();
            hamSwitch = false;
            bgUnFix();
        } else {
            $hamburger.addClass("is-active");
            // $navItem.css({ 'display': 'none' });
            $navItem.slideDown();
            hamSwitch = true;
            bgFix();
        }
    });
    //=== 手機版開合 ===
    // var $opSupNav = $('[opSupNav]');
    // $opSupNav.on('click', function () {
    //     windowWAct = $(window).width();
    //     if (windowWAct < 1201) {
    //         var $navSup = $(this).children('.n-nav-sup');
    //         $navSup.is(':visible') ? $navSup.slideUp() : $navSup.slideDown();
    //     }
    // });

    $("#goTop").on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    });
});

$(window).resize(function () {
    var windowW = $(window).width();

    if (windowW < windowRW) {
        hamSwitch ? $('#navItem').show() : $('#navItem').hide();
    } else {
        $('#navItem').css({ 'display': 'flex' });
        $('#hamBtn').removeClass("is-active");
        hamSwitch = false;
        // $('.n-nav-sup').removeAttr('style');
    }
});

$(window).scroll(function () {
    var windowScrollNum = $(window).scrollTop();
    if (windowScrollNum > 300) {
        $('#goTop').css({ 'bottom': '10px' });
        fixGoTop();
    } else {
        $('#goTop').css({ 'bottom': '-100px', 'top': 'auto' });
        $('#goTop').removeClass('gotop-fix');
    }
});

function fixGoTop() {
    var windowScrollNum = $(window).scrollTop();
    var windowH = $(window).height();
    var $footer = $('.footer-wrap');
    if ($footer.length) {
        var footerTop = $footer.offset().top;
        var $goTop = $('#goTop');
        if ($goTop.length) {
            var gotopH = $goTop.outerHeight();
            var gotopHhelf = gotopH / 2;
            //往上滑的高度 + 上螢幕高度 - gotop一半高 - bottom: 10px 的高度 > footer的距離body top 的高度
            if (windowScrollNum + windowH - gotopHhelf - 10 > footerTop) {
                $goTop.addClass('gotop-fix');
                $goTop.css({ 'bottom': 'auto', 'top': '-' + gotopHhelf + 'px' });
            } else {
                $goTop.removeClass('gotop-fix');
                $goTop.css({ 'bottom': '10px', 'top': 'auto' });
            }
        }
    }
}

function bgFix() {
    $('body').css({ 'overflow': 'hidden' });
}
function bgUnFix() {
    $('body').removeAttr('style');
}